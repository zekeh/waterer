
 import java.awt.*;
 import javax.swing.*;
 import java.awt.event.KeyEvent;
 import java.awt.event.KeyListener;
 import java.io.*;
 import java.awt.image.BufferedImage;
 import javax.imageio.ImageIO;

public class Waterer {

	private Game game;

	int x = 0;
	int y = 300;
	int xa = 0;
	int ya = 0;
	int WIDTH = 54;
	int HEIGHT = 51;

	private BufferedImage waterer;

	//the waterer starts with 3 lives
	private int lives = 3;

	public Waterer(Game game){
		this.game = game;
		
		try{
			File img = new File("images/waterer.bmp");

			waterer = ImageIO.read(img);
		}catch(IOException e){

		}

	}
	/**
	 * moves the waterer
	 */
	public void move() {
		if (x + xa > 0 && x + xa < game.getWidth())
			x = x + xa;
		if (y + ya < game.getHeight())
			y = y + ya;
	}

	public void paintComponent(Graphics2D g){
		g.drawImage(waterer, x, y, null);
	}

	/**
	 * changes to the value of xa and ya are invoked when keys are pressed, to move the waterer
	 * @param  e
	 */
	public void keyPressed(KeyEvent e) {
		Flower flower = new Flower(game, this);

	    switch (e.getKeyCode()) {
		   case KeyEvent.VK_UP:
		   	ya = -3;
		   	break;

		   case KeyEvent.VK_DOWN:
		   	ya = 3;
		   	break;

		   case KeyEvent.VK_LEFT:
		   	xa = -3;
		   	break;

		   case KeyEvent.VK_RIGHT:
		    xa = 3;
		   	break;

	   }
	}
	/**
	 * stops the movement when the keys are released
	 * @param e
	 */
	public void keyReleased(KeyEvent e){
		xa = 0;
		ya = 0;
	}

	/**
	 * gets the lives of the waterer
	 * @return lives
	 */
	public int getLives(){
		return lives;
	}
	/**
	 * sets the lives of the waterer
	 * @param lives
	 */
	public void setLives(int lives){
		this.lives = lives;
	}

	/**
	 * gets the bounds of the waterer
	 * @return rectangle of the bounds of the waterer
	 */
	public Rectangle getBounds() {
		return new Rectangle(x, y, WIDTH, HEIGHT);
	}
}