import java.awt.*;
import javax.swing.*;
import java.io.*;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.util.*;

public class Flower{
	private BufferedImage flowerAlive;
	private BufferedImage flowerDead;
	private BufferedImage flower;

	int x = 0;
	int y = 0;

	int WIDTH = 35;
	int HEIGHT = 33;

	Random random = new Random();

	private Game game;
 	private Waterer waterer;

 	public Flower(Game game, Waterer waterer){
 		//prevents them from spawning on the very outer edge
 		x = random.nextInt(620) + 10;
 		y = random.nextInt(460) + 10;

 		this.game = game;
 		this.waterer = waterer;


 		try{
			File img = new File("images/flowerAlive.bmp");
			File img2 = new File("images/flowerDead.bmp");

			flowerAlive = ImageIO.read(img);
			flowerDead = ImageIO.read(img2);
		}catch(IOException e){

		}
		//initially all flowers are dead
		setImage(flowerDead);
 	}

 	/**
 	 * waters the flowers if there has been a collision
 	 */
 	public void water(){
 		if(collision()){
 			setImage(flowerAlive);
 		}	
 	}

 	public void paintComponent(Graphics2D g){
 		g.drawImage(getImage(), x, y, null);
 	}

 	/**
 	 * gets the image of the flower
 	 * @return flowers
 	 */
 	public BufferedImage getImage() {
            return flower;
        }

    /**
     * sets the image of the flowers
     * @param flower
     */
    public void setImage(BufferedImage flower) {
            this.flower = flower;
        }

    /**
     * get the bounds of the flowers
     * @return a rectangle of the flower
     */
 	public Rectangle getBounds() {
		return new Rectangle(x, y, WIDTH, HEIGHT);
	}
	/**
	 * detects if there has been a collision between the waterer and the flower
	 * @return true if there has been a collision, false if not
	 */
	private boolean collision() {
		return game.character.getBounds().intersects(getBounds());
	}
}