
import java.awt.*;
import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.*;
import java.lang.*;
import java.net.URL;
//import java.awt.*;
import java.util.*;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;


class Game extends JPanel{

	Waterer character = new Waterer(this);
	ArrayList<Bees> bees = new ArrayList<Bees>();
	Flower[] flowers = new Flower[10];
	boolean[] compared = new boolean[10];
	private BufferedImage flowerAlive;

	public Game(){
		for(int i = 0; i < 10; i++){
			bees.add(new Bees(this, character));
			flowers[i] = new Flower(this, character);
		}

	   try{
			File img = new File("images/flowerAlive.bmp");
			flowerAlive = ImageIO.read(img);
		}catch(IOException e){

		}

	    addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
				character.keyReleased(e);
			}

			@Override
			public void keyPressed(KeyEvent e) {
				character.keyPressed(e);
			}
		});
		setFocusable(true);
	}
	/**
	 * moves all of the objects
	 */
	private void move(){
		character.move();
		for(Bees bee : bees) bee.move();
		for(Flower flower : flowers) flower.water();
		for(int i = 0; i < flowers.length; i++){
			compared[i] = compareImages(flowers[i].getImage(), flowerAlive);
		}
		if(areAllTrue(compared)){
			showMessage("You won!");
		}
	}
	/**
	 * Shows a message dialog of the chosen message
	 *
	 * @param message
	 */
	public void showMessage(String message) {
		JOptionPane.showMessageDialog(this, message, message, JOptionPane.YES_NO_OPTION);
		System.exit(ABORT);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.green);
		g2d.fillRect(0, 0, 640, 480);
		g2d.setColor(Color.red);
		g2d.setFont(new Font("TimesRoman", Font.PLAIN, 20));
		g2d.drawString("Lives: " + character.getLives(), 20, 20);

		for(Flower flower : flowers) flower.paintComponent(g2d);
		character.paintComponent(g2d);
		for(Bees bee : bees) bee.paintComponent(g2d);

		}
	/**
	 * Compares two images pixel by pixel.
	 *
	 * @param imgA the first image.
	 * @param imgB the second image.
	 * @return whether the images are both the same or not.
	 * @author http://stackoverflow.com/a/29886786/6853190
	 */
	public static boolean compareImages(BufferedImage imgA, BufferedImage imgB) {
	  // The images must be the same size.
	  if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {
	    int width = imgA.getWidth();
	    int height = imgA.getHeight();

	    // Loop over every pixel.
	    for (int y = 0; y < height; y++) {
	      for (int x = 0; x < width; x++) {
	        // Compare the pixels for equality.
	        if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {
	          return false;
	        }
	      }
	    }
	  } else {
	    return false;
	  }

	  return true;
}

	/**
	 *
	 * checks to see if all elements of the array are true
	 * @param  array
	 * @return       true if all are true, false if not
	 */
	public static boolean areAllTrue(boolean[] array)
	{
	    for(boolean b : array) {
	    	if(!b) {
	    	return false;
	    	}
		}
	    return true;
	}

	public static void main(String[] args) throws InterruptedException {
		JFrame frame = new JFrame("bees");
		Game game = new Game();
		frame.add(game);
		frame.setSize(640, 480);
		frame.setVisible(true);
		frame.setResizable(false);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		while (true) {
			game.move();
			game.repaint();
			Thread.sleep(10);
		}
	}
}
