import java.awt.*;
import javax.swing.*;
import java.io.*;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.util.*;

 public class Bees{

 	private BufferedImage bee;
 	Random random = new Random();
 	int x = 0;
	int y = 0;
	int ya;
	int xa;
	int WIDTH = 22;
	int HEIGHT = 22;
	Boolean colliding;
	Boolean invincible = false;

 	private Game game;
 	private Waterer waterer;

 	public Bees(Game game, Waterer waterer){
 		x = random.nextInt(640);
 		y = random.nextInt(480);
 		xa = 1;
		ya = 1;

 		this.game = game;
 		this.waterer = waterer;
 		try{
			File img = new File("images/bee.bmp");

			bee = ImageIO.read(img);
		}catch(IOException e){

		}

 	}
 	/**
 	 * moves the bees
 	 */
 	public void move(){
 		if (x + xa < 0)
			xa = 1;
		if (x + xa > game.getWidth() - WIDTH)
			xa = -1;
		if (y + ya < 0)
			ya = 1;
		if (y + ya > game.getHeight() - HEIGHT)
			ya = -1;

		if (collision()){
			colliding = true;
		}
		else{
			colliding = false;
		}
		if(!invincible){
			if(colliding){
				//a live is lost when there has been a collision
				int lives = waterer.getLives() - 1;
				waterer.setLives(lives);
				invincible = true;
			}
		}
		if(waterer.getLives() == 0){
			//gameover if all lives have been lost
			game.showMessage("You lost.");
		}
		x = x + xa;
		y = y + ya;

 	}
 	/**
 	 * detects collision
 	 * @return true if there has been a collision. false if not
 	 */
 	private boolean collision() {
		return game.character.getBounds().intersects(getBounds());
	}
 	public void paintComponent(Graphics2D g){
 		g.drawImage(bee, x, y, null);
 	}

 	/**
 	 * gets the bounds of the bees
 	 * @return rectangle of the bounds
 	 */
 	public Rectangle getBounds() {
		return new Rectangle(x, y, WIDTH, HEIGHT);
	}

 }
